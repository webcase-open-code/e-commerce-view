from django.utils.translation import ugettext as _
from django.views.generic import ListView, DetailView, ModelFormView

from core.views import BaseView
from shop.models import Promo, Product, OrderProduct, Comment
from shop.forms import DeleteCartItemForm
from shop.filtersets import ProductFilterSet
from pages.views.mixins import SEOViewMixin


__all__ = (
    'PromoListView',
    'PromoDetail',
    'ProductDetail',
    'ProductList',
    'OrderProcessFormView'
)


class PromoListView(ListView, BaseView):
    """
    View for displaying list of promos.
    """

    template_name = 'shop/promo_list.jinja'
    queryset = Promo.objects.is_active().prefetch_related('translations')


class PromoDetail(DetailView, BaseView):
    """
    View for displaying promo info.
    """

    template_name = 'shop/promo_detail.jinja'
    queryset = Promo.objects.is_active().prefetch_related('translations')


class ProductDetail(DetailView, BaseView):
    """
    View for displaying product info.
    """

    template_name = 'shop/product_detail.jinja'
    queryset = Product.objects.select_related('comments')


class ProductList(SEOViewMixin, ListView):
    """
    View for displaying list of products.
    """

    template_name = 'shop/product_list.jinja'
    model = Product
    page_name = SEO.CATALOG
    filterset_class = ProductFilterSet
    order_dict = {
        '-created_at': _('Newest'),
        'actual_price': _('Price (low-high)'),
        '-actual_price': _('Price (high-low)'),
        'name': _('Name (A-Z)'),
        '-name': _('Name (Z-A)')
    }

    def order_queryset(self, queryset):
        """
        Used to sort products by the request parameters.
        The default sorting is by creation date.

        Attrs:
            queryset (QuerySet): Queryset of products.

        Returns:
            QuerySet: Ordered queryset of products.
        """

        order = self.request.GET.get('order', '-created_at')

        if order not in self.order_dict:
            order = '-created_at'

        return queryset.order_by(order)

    def get_queryset(self):
        """
        Returns queryset of products with actual prices.

        Returns:
            QuerySet: products with actual prices.
        """

        return self.model.objects.actual_price().prefetch_related('translations')

    def get_context_data(self, **kwargs):
        """
        Adds all the context data needed in view's template.

        Args:
            **kwargs (dict): Context data.

        Returns:
            dict: context data.
        """

        kwargs['object_list'] = (
            self.order_queryset(kwargs['object_list']).distinct()
        )
        kwargs['slider_list'] = Slide.objects.filter(page=Slide.CATALOG)
        kwargs['orders_dict'] = self.order_dict

        return super(ProductList, self).get_context_data(**kwargs)


class OrderProcessFormView(LoginRequiredMixin, BaseView, ModelFormView):
    """
    Process user order view.
    """
    template_name = 'shop/order_process.jinja'
    form_class = OrderProcessForm
    success_url = reverse_lazy('pages:index')

    def get_form_kwargs(self):
        """Updates form kwargs with the current request instance to
        process cart in the form.

        Returns:
            Dict: Form parameters to pass.
        """

        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request

        return kwargs

    def get_initial(self):
        """Updates form initial data with user data.

        Returns:
            Dict: Form initial data.
        """

        user = self.request.user

        return {
            'contact_person': user.profile.contact_person,
            'phone': user.profile.phone,
            'email': user.email,
        }
