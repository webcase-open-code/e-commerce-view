# Views.py

A chunk of code from our e-commerce project. All the data is translated using plugin `django-parler` and default django internationalization techniques. Filtering is implemented using `django-filter`

In the file you can see:

* Promos - ListView and DetailView of the shop active promos.

* Products - DetailView and ListView

* Order process - ModelFormView to process order.
